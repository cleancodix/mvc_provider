# mvcprovider

If you come from *version 1*, please look at the **changelog** to handle the *V2* **breaking changes**.

Create a ***simple MVC*** architecture based on the ***Provider*** package.
The most recommended *state management* package by Flutter founders.

- Start creating value fast with a clear and well structured code.
    - Module
        - Model
        - View
        - Controller
        
- Simplest lifecycle management from the **controller** AND/OR the **model**.
    - create() // Called once before first view rendering
    - update() // Called every view rendering
     
- The easiest way to inject **dependencies**, **services** or **providers**. You just have to create a class which extend ***MVC_Notifier*** or ***MVC_Provider***
```
   // In the module
   // You can inject all providers that you will use in children:
   final List<SingleChildWidget> providers = [
     MyThemeService().create,
     MyUserStateService().create,
   ];
```

- Access to your previously declared **dependencies** just by calling
  **MyThemeService(context).listen** or **MyThemeService(context).get**
  anywhere in your code

## Getting Started with the official Flutter example

Everybody need to create a **counter module**,
and of course you need it too.

## STEP 1

First, define your very complex **model** (*my.counter.model.dart*)  :

```
    class MyCounterModel extends MVC_Model<MyCounterCtrl> {
    
       int _count = 0;
       set count(int value) {
         _count = value;
         notifyListeners();
       }
       int get count => _count;
    }
```

## STEP 2

Then define a **controller** that will increment the *count* property
with a lot of mathematics operations (*my.counter.ctrl.dart*)  :

```
    class MyCounterCtrl extends MVC_Controller<MyCounterModel> {
    
       void increment() {
         model.count++;
       }
    }
```

For now, you have an amazing method *increment* that will increment (no ?! really ??)
you're model's property *count* which will call *refresh* (a method alias for the method *notifyListeners* inherited from her mother class **ChangeNotifier**)
in order to refresh the **view**.

## STEP 3

But to refresh the **view**. You need a **view**. Here it is (*my.counter.view.dart*)  :

```
    class MyCounterView extends StatelessWidget with MVC_View<MyCounterModel, MyCounterCtrl> {
          @override
          Widget build(BuildContext context, [Widget? child]) {
            listen(context);
        
            return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Counter : ${model.count}"),
                  RaisedButton(child: Text("Increment"), onPressed: ctrl.increment)
                ]
            );
          }
        }
```

It's a bit complicated **view**. I'll explain : it's a *text* on top of a *button*.

Like you can see the *text* display our **model**'s property *count* and the
*button* execute our **controller**'s method *increment* when pressed.

## STEP 4

In order to make the magic happen you'll need to link these **three entities**
together, I decided that this fusion will be called a **Module** but you can
call this **Roberto** or anything else if you prefer.

Here is our **module** (*my.counter.dart*) :

```
    class MyCounterModule extends MVC_Module<MyCounterModel, MyCounterView, MyCounterCtrl> {
    
       final MyCounterModel model = MyCounterModel();
       final MyCounterView view = MyCounterView();
       final MyCounterCtrl ctrl = MyCounterCtrl();
       final List<SingleChildWidget> providers = [];
    }
```

## STEP 5

To test you newly counter feature in your app just call your widget ***MyCounterModule()***
Example in your *main.dart* file :

```
    void main() => runApp(AmazingSample());
    
    class AmazingSample extends StatelessWidget {
      // This widget is the root of this amazing package sample.
      @override
      Widget build(BuildContext context) {
        return MaterialApp(
          title: 'MVC Provider counter demo',
          home: MyCounterModule(),
        );
      }
    }
```

At this point you should have a *main.dart* file and a ***mycounter*** folder with 4 files inside :

- >The module ***my.counter.dart***
- >The model ***my.counter.model.dart***
- >The view ***my.counter.view.dart***
- >The controller ***my.counter.ctrl.dart***