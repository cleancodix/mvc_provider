# mvcprovider

Lean how to use *Flutter* with the ***mvcprovider*** package with a simple counter example:

## Simple example - Simple Counter App

Please follow this link to find a simple example :
<a href="https://gitlab.com/mrdiez/mvc_provider/blob/master/example/simple_example" target="_blank">Simple example source code</a>

## Simple example : main.dart

```
    import 'package:flutter/material.dart';
    import 'features/mycounter/my.counter.dart';
    
    void main() => runApp(AmazingSample());
    
    class AmazingSample extends StatelessWidget {
      // This widget is the root of this amazing package sample.
      @override
      Widget build(BuildContext context) {
        return MaterialApp(
          title: 'MVC Provider counter demo',
          home: MyCounterModule(),
        );
      }
    }
```

## Simple example : modules/mycounter/my.counter.dart

```
    import 'package:mvcprovider/mvcprovider.dart';
    import 'my.counter.ctrl.dart';
    import 'my.counter.model.dart';
    import 'my.counter.view.dart';
    
    class MyCounterModule extends MVC_Module<MyCounterModel, MyCounterView, MyCounterCtrl> {
      final MyCounterModel model = MyCounterModel();
      final MyCounterView view = MyCounterView();
      final MyCounterCtrl ctrl = MyCounterCtrl();
      final List<SingleChildWidget> providers = [];
    }
```

## Simple example : modules/mycounter/my.counter.model.dart

```
    import 'package:mvcprovider/mvcprovider.dart';
    import 'my.counter.ctrl.dart';
    
    class MyCounterModel extends MVC_Model<MyCounterCtrl> {
      int _count = 0;
      set count(int value) {
        _count = value;
        notifyListeners();
      }
      int get count => _count;
    }
```

## Simple example : modules/mycounter/my.counter.ctrl.dart

```
    import 'package:mvcprovider/mvcprovider.dart';
    import 'my.counter.model.dart';
    
    class MyCounterCtrl extends MVC_Controller<MyCounterModel> {
      void increment() {
        model.count++;
      }
    }
```

## Simple example : modules/mycounter/my.counter.view.dart

```
    import 'package:flutter/material.dart';
    import 'package:mvcprovider/mvcprovider.dart';
    import 'my.counter.ctrl.dart';
    import 'my.counter.model.dart';
    
    class MyCounterView extends StatelessWidget with MVC_View<MyCounterModel, MyCounterCtrl> {
      @override
      Widget build(BuildContext context, [Widget? child]) {
        listen(context);
    
        return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Counter : ${model.count}"),
              RaisedButton(child: Text("Increment"), onPressed: ctrl.increment)
            ]
        );
      }
    }
```
