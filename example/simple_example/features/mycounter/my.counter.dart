import '../../../../lib/mvcprovider.dart';
import 'my.counter.ctrl.dart';
import 'my.counter.model.dart';
import 'my.counter.view.dart';

class MyCounterModule extends MVC_Module<MyCounterModel, MyCounterView, MyCounterCtrl> {
  final MyCounterModel model = MyCounterModel();
  final MyCounterView view = MyCounterView();
  final MyCounterCtrl ctrl = MyCounterCtrl();
  final List<SingleChildWidget> providers = [];
}
