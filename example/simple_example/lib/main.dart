import 'package:flutter/material.dart';
import 'features/mycounter/my.counter.dart';

void main() => runApp(AmazingSample());

class AmazingSample extends StatelessWidget {
  // This widget is the root of this amazing package sample.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MVC Provider counter demo',
      home: MyCounterModule(),
    );
  }
}
